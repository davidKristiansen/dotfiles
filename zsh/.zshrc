source $HOME/.antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle git-flow
antigen bundle pip
antigen bundle command-not-found
antigen bundle docker
antigen bundle history-substring-search
antigen bundle history
antigen bundle vi-mode
antigen bundle pyenv

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-completions

antigen bundle "MichaelAquilina/zsh-autoswitch-virtualenv"

# Load the theme.
antigen theme robbyrussell


# Tell Antigen that you're done.
antigen apply

# VTE terminal fix
# https://gnunn1.github.io/tilix-web/manual/vteconfig/
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi

## Alias
alias :q=exit
alias :Q=:q
alias :q!=:q
alias :Q!=:q
alias :wq=:q
alias Wq=:q
