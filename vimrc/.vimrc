
if executable('tmux') && filereadable(expand('~/.zshrc')) && $TMUX !=# ''
    let g:vimIsInTmux = 1
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
else
    let g:vimIsInTmux = 0
endif

"*****************************************************************************
"" Plug
"*****************************************************************************"
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


call plug#begin()

"" NERDTree
Plug 'preservim/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'albfan/nerdtree-git-plugin'

"" Look
Plug 'ryanoasis/vim-devicons'
Plug 'sainnhe/gruvbox-material'
Plug 'sainnhe/artify.vim'

"" Line
Plug 'itchyny/lightline.vim'
Plug 'shinchu/lightline-gruvbox.vim'
Plug 'albertomontesg/lightline-asyncrun'
Plug 'macthecadillac/lightline-gitdiff'

"" Pomodoro
Plug 'rmolin88/pomodoro.vim'

if g:vimIsInTmux == 1
    Plug 'sainnhe/tmuxline.vim', { 'on': [ 'Tmuxline', 'TmuxlineSnapshot' ] }
endif
"" Git
Plug 'tpope/vim-fugitive'
Plug 'itchyny/vim-gitbranch'

"" Vim-Session
Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'

"" Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

"" Syntax
Plug 'sheerun/vim-polyglot'

"" Search
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

"" Commenter
Plug 'tomtom/tcomment_vim'

"" Autocomplete
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"" Python

"" Tmux
Plug 'christoomey/vim-tmux-navigator'

call plug#end()

" Required
filetype plugin indent on

"*****************************************************************************
"" Basic Setup
"*****************************************************************************
"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

"" unclutter workspace
set backupdir=/tmp//
set directory=/tmp//
set undodir=/tmp//

"" Enable Mouse
set mouse=a
set ttymouse=xterm2

"" Fix backspace indent
set backspace=indent,eol,start

"" Tabs. May be overridden by autocmd rules
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab

"" Map leader to ,
let mapleader=','

"" Enable hidden buffers
set hidden

"" Split direction
set splitbelow
set splitright

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

set fileformats=unix,dos,mac

if exists('$SHELL')
    set shell=$SHELL
else
    set shell=/bin/sh
endif

" session management
"let g:session_directory = "{{.Config.BaseDir}}/session"
let g:session_autoload = "no"
let g:session_autosave = "no"
let g:session_command_aliases = 1

"" Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

"" Change cursor in insert mode (Gnome terminal)
"" https://vim.fandom.com/wiki/Change_cursor_shape_in_different_modes
if has("autocmd")
  au VimEnter,InsertLeave * silent execute '!echo -ne "\e[1 q"' | redraw!
  au InsertEnter,InsertChange *
    \ if v:insertmode == 'i' |
    \   silent execute '!echo -ne "\e[5 q"' | redraw! |
    \ elseif v:insertmode == 'r' |
    \   silent execute '!echo -ne "\e[3 q"' | redraw! |
    \ endif
  au VimLeave * silent execute '!echo -ne "\e[ q"' | redraw!
endif

"*****************************************************************************
"" Visual Settings
"*****************************************************************************
syntax on
set ruler
set relativenumber

" always show signcolumns
set signcolumn=yes
highlight clear SignColumn

set laststatus=2

" Better display for messages
set cmdheight=2

" Hide mode message from statusline
set noshowmode

" important!!
set termguicolors

set background=dark

let g:gruvbox_material_background = 'hard'

colorscheme gruvbox-material

let g:lightline = {
      \ 'colorscheme': 'gruvbox',
      \ }

set t_Co=256



"*****************************************************************************
"" Key mapping
"*****************************************************************************
"" no one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall

nmap <leader>n :NERDTreeToggle<cr>

"*****************************************************************************
"" Plugin Settings
"*****************************************************************************

" Fzf
" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1

let g:fzf_action = {
      \ 'ctrl-s': 'split',
      \ 'ctrl-v': 'vsplit'
      \ }
nnoremap <c-p> :FZF<cr>
augroup fzf
  autocmd!
  autocmd! FileType fzf
  autocmd  FileType fzf set laststatus=0 noshowmode noruler
    \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler
augroup END

" NERDTree
" Open NERDTree when no file
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") && v:this_session == "" | NERDTree | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
" Close NERDTree when only window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Ultisnips
" remove <tab> because COC is using it
let g:UltiSnipsExpandTrigger = '<f5>'

" COCNvim
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" Use <Tab> and <S-Tab> to navigate the completion list:
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Use <cr> to confirm completion
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Close the preview window when completion is done.
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Lightline
function! SwitchLightlineColorScheme(color)
    let g:lightline.colorscheme = a:color
    call lightline#init()
    call lightline#colorscheme()
    call lightline#update()
endfunction

function! PomodoroStatus() abort
    if pomo#remaining_time() ==# '0'
        return "\ue001"
    else
        return "\ue003 ".pomo#remaining_time()
    endif
endfunction

function! CocCurrentFunction()
    return get(b:, 'coc_current_function', '')
endfunction

function! Devicons_Filetype()
    " return winwidth(0) > 70 ? (strlen(&filetype) ? WebDevIconsGetFileTypeSymbol() . ' ' . &filetype : 'no ft') : ''
    return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction

function! Devicons_Fileformat()
    return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
endfunction

function! Artify_active_tab_num(n) abort
    return Artify(a:n, 'bold')." \ue0bb"
endfunction

function! Artify_inactive_tab_num(n) abort
    return Artify(a:n, 'double_struck')." \ue0bb"
endfunction

function! Artify_lightline_tab_filename(s) abort
    return Artify(lightline#tab#filename(a:s), 'monospace')
endfunction

function! Artify_lightline_mode() abort
    return Artify(lightline#mode(), 'monospace')
endfunction

function! Artify_line_percent() abort
    return Artify(string((100*line('.'))/line('$')), 'bold')
endfunction

function! Artify_line_num() abort
    return Artify(string(line('.')), 'bold')
endfunction

function! Artify_col_num() abort
    return Artify(string(getcurpos()[2]), 'bold')
endfunction

function! Artify_gitbranch() abort
    if gitbranch#name() !=# ''
        return Artify(gitbranch#name(), 'monospace')." \ue725"
    else
        return "\ue61b"
    endif
endfunction

augroup lightlineCustom
    autocmd!
    autocmd BufWritePost * call lightline_gitdiff#query_git() | call lightline#update()
augroup END
let g:lightline = {}
let g:lightline.separator = { 'left': "\ue0b8", 'right': "\ue0be" }
let g:lightline.subseparator = { 'left': "\ue0b9", 'right': "\ue0b9" }
let g:lightline.tabline_separator = { 'left': "\ue0bc", 'right': "\ue0ba" }
let g:lightline.tabline_subseparator = { 'left': "\ue0bb", 'right': "\ue0bb" }
let g:lightline#ale#indicator_checking = "\uf110"
let g:lightline#ale#indicator_warnings = "\uf529"
let g:lightline#ale#indicator_errors = "\uf00d"
let g:lightline#ale#indicator_ok = "\uf00c"
let g:lightline_gitdiff#indicator_added = '+'
let g:lightline_gitdiff#indicator_deleted = '-'
let g:lightline_gitdiff#indicator_modified = '*'
let g:lightline_gitdiff#min_winwidth = '70'
let g:lightline#asyncrun#indicator_none = ''
let g:lightline#asyncrun#indicator_run = 'Running...'


let g:lightline.active = {
    \ 'left': [ [ 'artify_mode', 'paste' ],
    \           [ 'readonly', 'filename', 'modified', 'fileformat', 'devicons_filetype' ] ],
    \ 'right': [ [ 'artify_lineinfo' ],
    \            [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_ok', 'pomodoro' ],
    \           [ 'asyncrun_status', 'coc_status' ] ]
    \ }

let g:lightline.inactive = {
    \ 'left': [ [ 'filename' , 'modified', 'fileformat', 'devicons_filetype' ]],
    \ 'right': [ [ 'artify_lineinfo' ] ]
    \ }
let g:lightline.tabline = {
    \ 'left': [ [ 'vim_logo', 'tabs' ] ],
    \ 'right': [ [ 'artify_gitbranch' ],
    \ [ 'gitstatus' ] ]
    \ }
let g:lightline.tab = {
    \ 'active': [ 'artify_activetabnum', 'artify_filename', 'modified' ],
    \ 'inactive': [ 'artify_inactivetabnum', 'filename', 'modified' ] }
let g:lightline.tab_component = {
    \ }
let g:lightline.tab_component_function = {
    \ 'artify_activetabnum': 'Artify_active_tab_num',
    \ 'artify_inactivetabnum': 'Artify_inactive_tab_num',
    \ 'artify_filename': 'Artify_lightline_tab_filename',
    \ 'filename': 'lightline#tab#filename',
    \ 'modified': 'lightline#tab#modified',
    \ 'readonly': 'lightline#tab#readonly',
    \ 'tabnum': 'lightline#tab#tabnum'
    \ }
let g:lightline.component = {
    \ 'artify_gitbranch' : '%{Artify_gitbranch()}',
    \ 'artify_mode': '%{Artify_lightline_mode()}',
    \ 'artify_lineinfo': "%2{Artify_line_percent()}\uf295 %3{Artify_line_num()}:%-2{Artify_col_num()}",
    \ 'gitstatus' : '%{lightline_gitdiff#get_status()}',
    \ 'bufinfo': '%{bufname("%")}:%{bufnr("%")}',
    \ 'vim_logo': "\ue7c5",
    \ 'pomodoro': '%{PomodoroStatus()}',
    \ 'mode': '%{lightline#mode()}',
    \ 'absolutepath': '%F',
    \ 'relativepath': '%f',
    \ 'filename': '%t',
    \ 'filesize': "%{HumanSize(line2byte('$') + len(getline('$')))}",
    \ 'fileencoding': '%{&fenc!=#""?&fenc:&enc}',
    \ 'fileformat': '%{&fenc!=#""?&fenc:&enc}[%{&ff}]',
    \ 'filetype': '%{&ft!=#""?&ft:"no ft"}',
    \ 'modified': '%M',
    \ 'bufnum': '%n',
    \ 'paste': '%{&paste?"PASTE":""}',
    \ 'readonly': '%R',
    \ 'charvalue': '%b',
    \ 'charvaluehex': '%B',
    \ 'percent': '%2p%%',
    \ 'percentwin': '%P',
    \ 'spell': '%{&spell?&spelllang:""}',
    \ 'lineinfo': '%2p%% %3l:%-2v',
    \ 'line': '%l',
    \ 'column': '%c',
    \ 'close': '%999X X ',
    \ 'winnr': '%{winnr()}'
    \ }
let g:lightline.component_function = {
    \ 'gitbranch': 'gitbranch#name',
    \ 'devicons_filetype': 'Devicons_Filetype',
    \ 'devicons_fileformat': 'Devicons_Fileformat',
    \ 'coc_status': 'coc#status',
    \ 'coc_currentfunction': 'CocCurrentFunction'
    \ }
let g:lightline.component_expand = {
    \ 'linter_checking': 'lightline#ale#checking',
    \ 'linter_warnings': 'lightline#ale#warnings',
    \ 'linter_errors': 'lightline#ale#errors',
    \ 'linter_ok': 'lightline#ale#ok',
    \ 'asyncrun_status': 'lightline#asyncrun#status'
    \ }
let g:lightline.component_type = {
    \ 'linter_warnings': 'warning',
    \ 'linter_errors': 'error'
    \ }
let g:lightline.component_visible_condition = {
    \ 'gitstatus': 'lightline_gitdiff#get_status() !=# ""'
    \ }

" Tmuxline
if g:vimIsInTmux == 1
    let g:tmuxline_preset = {
        \'a'    : '#S',
        \'b'    : '%R',
        \'c'    : [ '#{sysstat_mem} #[fg=blue]\ufa51#{upload_speed}' ],
        \'win'  : [ '#I', '#W' ],
        \'cwin' : [ '#I', '#W', '#F' ],
        \'x'    : [ "#[fg=blue]#{download_speed} \uf6d9 #{sysstat_cpu}" ],
        \'y'    : [ '%a' ],
        \'z'    : '#H #{prefix_highlight}'
        \}
    let g:tmuxline_separators = {
        \ 'left' : "\ue0bc",
        \ 'left_alt': "\ue0bd",
        \ 'right' : "\ue0ba",
        \ 'right_alt' : "\ue0bd",
        \ 'space' : ' '}
endif
highlight clear SignColumn




let g:python_highlight_all = 1
